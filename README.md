# TubeBeacon

Monitoring script that will fire off a message to a Discord webhook whenever one or more YouTube channels upload a new video

The file "sample_config.json" must be copied over to a new file called "config.json" in the same directory (alternatively, a path to the config file may be passed as the first parameter when the script is executed), which must contain a Discord webhook URL and a valid YouTube Data API key in order for this script to function. A list of channels must also be specified--check the sample config to see the format for this. This script needs to be placed into the crontab (ideally, a shell script that calls this script should be run by cron to avoid directory errors) and run as frequently as the lowest minute delay in the channel config.
