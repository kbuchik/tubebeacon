#!/usr/bin/env python

# tubebeacon.py -- A cron script for monitoring YouTube channels for new uploads and sending signals to Discord webhooks
# Author: Kevin Buchik <kdbuchik@gmail.com>

import datetime
import discord
import json
import os
import requests
import sys
import time
from discord import Webhook, RequestsWebhookAdapter, File

CONFIG = STATUS = dict()
# Number of seconds to "buffer" update checks against the delay to compensate for the time it takes to execute the program
GRACE_SECS = 5
# Log file handle
LOGFILE = None
# True if a log file is defined in the config
LOGGING = False
# Discord webhook (initialized in sendWebhookMessage() the first time webhook is used)
WEBHOOK = None

def main():
    global LOGFILE,LOGGING,STATUS
    confpath = "config.json"
    if len(sys.argv) > 1:
        if sys.argv[1] == '-m' or sys.argv[1] == '--msg' or sys.argv[1] == '--message':
            if len(sys.argv) > 2:
                loadConfig(confpath)
                sendWebhookMessage(sys.argv[2])
            exit(0)
        else:
            confpath = sys.argv[1]
    loadConfig(confpath)
    if len(sys.argv) > 2:
        if sys.argv[2] == '-m' or sys.argv[2] == '--msg' or sys.argv[2] == '--message':
            if len(sys.argv) > 3:
                sendWebhookMessage(sys.argv[3])
                exit(0)
    if 'logfile' in CONFIG:
        if len(CONFIG['logfile']) > 0:
            LOGGING = True
            if not os.path.isfile(CONFIG['logfile']):
                LOGFILE = open(CONFIG['logfile'], 'w+')
                writeLog("Log file '" + CONFIG['logfile'] + "' initialized")
            else:
                LOGFILE = open(CONFIG['logfile'], 'a')
    if os.path.isfile(CONFIG['statusFile']):
        try:
            STATUS = json.loads(open(CONFIG['statusFile']).read())
        except json.JSONDecodeError as ex:
            print("Error: malformed status file\n" + str(ex))
            exit(1)
    if len(CONFIG['channels']) == 0:
        print("No channels defined in config file!")
        exit(0)
    
    # Main loop through channels in config file
    for chan in CONFIG['channels']:
        if chan['id'] in STATUS:
            # Channel is defined in the status file and has been checked before
            if getCurrentTime() >= strToUnixTime(STATUS[chan['id']]['lastCheck']) + (chan['delay'] * 60) - GRACE_SECS:
                # More time has passed than the set delay; check for new videos
                lastId = getNewestVidIdFromWeb(chan['id'])
                stat = STATUS[chan['id']]
                # We update the time on 'lastCheck' in this case
                STATUS[chan['id']]['lastCheck'] = getCurrentTime(False)
                print("Check ran at " + getCurrentTime(False) + "; lastId: " + lastId + " last vid ID from status file: " + stat['lastVideo'])
                if '$FAIL$' in lastId:
                    print("Failed to grab most recent video ID with web requests for channel " + chan['id'] + " with error " + lastId)
                if lastId != stat['lastVideo'] and '$FAIL$' not in lastId:
                    vid = getVideoData(lastId)
                    if vid:
                        if strToUnixTime(vid['published']) > strToUnixTime(stat['lastUpdate']):
                            # In the following block, the data in "vid" describes a video that has not yet been seen by the script; any action can be taken here
                            # In this case we send a message to the webhook
                            url = "https://www.youtube.com/watch?v=" + vid['id']
                            sendWebhookMessage(chan['name'] + " has uploaded a new video titled \"" + vid['title'] + "\"\n" + url)
                            print("Reported new video with ID " + vid['id'] + " from channel " + chan['name'])
                            writeLog("New video '" + vid['title'] + "' (ID " + vid['id'] + ") published on channel " + chan['name'] + " (ID " + chan['id'] + ") at " + vid['published'] + " (message sent to Discord webhook ID " + CONFIG['webhookID'] + ")")
                            
                            # Update status dict
                            STATUS[chan['id']]['lastVideo'] = vid['id']
                            STATUS[chan['id']]['lastUpdate'] = vid['published']
                            STATUS[chan['id']]['lastCheck'] = getCurrentTime(False)
        else:
            # Channel is not defined in the status file and thus has never been checked; initialize its entry in the status file (no messages will be sent)
            timestamp = getCurrentTime(False)
            vidlist = getVideoList(chan['id'], 1)
            cStat = dict()
            if len(vidlist) == 0:
                cStat = {'lastVideo': 'NULL', 'lastUpdate_string': timestamp, 'lastCheck_string': timestamp, 'name': chan['name']}
                writeLog("Status record for channel '" + chan['name'] + "' with ID " + chan['id'] + " initialized with video ID " + "NULL")
            else:
                vid = vidlist[0]
                cStat = {'lastVideo': vid['id']['videoId'], 'lastUpdate': convertGoogleTime(vid['snippet']['publishedAt']), 'lastCheck': timestamp, 'name': chan['name']}
                writeLog("Status record for channel '" + chan['name'] + "' with ID " + chan['id'] + " initialized with video ID " + vid['id']['videoId'])
            STATUS[chan['id']] = cStat
    
    saveStatusFile()
    if LOGGING:
        LOGFILE.close()
    exit(0)

# Returns a reduced list of videos on the given channel with the last video being lastVid (if it exists in the list; otherwise it will go through the whole page)
# If "lastVid" is NULL, then returns only the first vid in the list
# If the list is empty, returns an empty list
# List items are dicts of the form {'id','title','published'}
# 'vidlist' is the 'items' array from the deserialized request JSON, and 'lastVid' is the ID of "lastvideo" from the status dict (or NULL)
## UNUSED FUNCTION
'''
def refineVideoList(vidlist, lastVid):
    rlist = list()
    for vid in vidlist:
        rlist.append({'id': vid['id']['videoId'], 'title': vid['snippet']['title'], 'published': convertGoogleTime(vid['snippet']['publishedAt'])})
        if lastVid == "NULL" or vid['id']['videoId'] == lastVid:
            return rlist
    return rlist
'''

# Returns a list (deserialized from the JSON string) of the first 'num' video(s) from the given channel using the YouTube data API
def getVideoList(channel, num):
    url = 'https://www.googleapis.com/youtube/v3/search'
    prm = {'channelId': channel, 'part': 'snippet,id', 'order': 'date', 'maxResults': num, 'key': CONFIG['youTubeKey']}
    req = requests.get(url, params=prm)
    if req.status_code == 200:
        response = json.loads(req.text)
        return response['items']
    else:
        print("Error in retrieving recent videos list for channel " + channel + "; request response:\n" + req.text)
        print("Possible that channel ID is malformed in config file, or the specified channel doesn't exist anymore?")
        exit(2)

# Returns a dict in the form {'id','title','published','channel'} for the given video, using API search (returns empty dict on fail)
def getVideoData(vid):
    url = 'https://www.googleapis.com/youtube/v3/videos'
    prm = {'id': vid, 'part': 'snippet', 'key': CONFIG['youTubeKey']}
    req = requests.get(url, params=prm)
    if req.status_code == 200:
        response = json.loads(req.text)
        try:
            obj = response['items'][0]
            return {'id': obj['id'], 'title': obj['snippet']['title'], 'published': convertGoogleTime(obj['snippet']['publishedAt']), 'channel': obj['snippet']['channelId']}
        except IndexError as ex:
            print("getVideoData() encountered an IndexError on attempting to pull video details on " + vid + " from API; response content:")
            print(req.text)
            return dict()
    else:
        print("Error: getVideoData() encountered a " + str(req.status_code) + " error while trying to get info for video " + vid + ":")
        print(req.text)
        return dict()


# Given the channel ID, will return the ID of the most recent video using web requests (instead of API queries)
def getNewestVidIdFromWeb(channel):
    url = 'https://www.youtube.com/channel/' + channel + '/videos'
    user_agent = {'User-agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0'}
    req = requests.get(url, headers = user_agent)
    if req.status_code == 200:
        resp = req.text
        # Check if this is an invalid channel
        if '<div class="channel-empty-message banner-message">' in resp:
            return 'INVALID CHANNEL'
        # Locate the line that matches the JS object we want
        trigger = 'window["ytInitialData"]'
        lines = resp.splitlines()
        for l in lines:
            if trigger in l:
                j = l.strip()
                j = json.loads(j[j.find('{'):-1])
                try:
                    return j['contents']['twoColumnBrowseResultsRenderer']['tabs'][1]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['gridRenderer']['items'][0]['gridVideoRenderer']['videoId']
                except KeyError as ex:
                    return '$FAIL$ KEY ERROR'
        return '$FAIL$ TARGET JS OBJ NOT FOUND'
    else:
        print("Error: getNewestVidIdFromWeb() couldn't load URL " + url)
        return '$FAIL$ HTTP ERROR'

# Load the config file at "confpath" into the global varible "CONFIG"
def loadConfig(confpath):
    global CONFIG
    if not os.path.isfile(confpath):
        print("Error: config file '" + confpath + "' not found")
        #print("Error: A config.json file must either exist in the same folder as tubebeacon.py, or a path to an alternative file must be passed as the first argument.\nTry renaming the sample_config.json file included in the repository, and configure it with your own channels before running again.")
        exit(1)
    try:
        CONFIG = json.loads(open(confpath, 'r').read())
        try:
            whUrl = CONFIG['webhookUrl'].split("/")
            CONFIG['webhookID'] = whUrl[5]
            CONFIG['webhookToken'] = whUrl[6]
        except KeyError as ex:
            print("Error: key 'webhookUrl' doesn't exist in config, or URL is invalid")
            exit(1)
    except json.JSONDecodeError as ex:
        print("Error attempting to parse config json file " + confpath + ": " + str(ex))
        exit(1)
    pass

# Remove channels that are no longer in the config file from the status dict, and save the status file
def saveStatusFile():
    global STATUS
    # Remove any channels from STATUS if they are no longer defined in CONFIG
    channels = list()
    for chan in CONFIG['channels']:
        channels.append(chan['id'])
    for chan in STATUS:
        if chan not in channels:
            del STATUS[chan]
            writeLog("Removed record for channel " + chan + " as it no longer exists in the config file")
    # Save STATUS dict
    try:
        statfile = open(CONFIG['statusFile'], 'w')
        statfile.write(json.dumps(STATUS))
        statfile.close()
    except IOError as ex:
        print("Error saving status file: " + str(ex))

# Send the string "msg" to the Discord webhook defined in the config file
def sendWebhookMessage(msg):
    global WEBHOOK
    if WEBHOOK is None:
        WEBHOOK = Webhook.partial(CONFIG['webhookID'], CONFIG['webhookToken'], adapter=RequestsWebhookAdapter())
    WEBHOOK.send(msg)

## Time functions

# Returns the current time either in unix timestamp (if "unixtime" is true), or in YYYY-mm-dd HH:MM:SS format [defaults to unix time]
def getCurrentTime(unixtime = True):
    return (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), int(time.time()))[unixtime]

# Converts a string in the form "YYYY-mm-dd HH:MM:SS" to unix timestamp integer
def strToUnixTime(timestring):
    return int(datetime.datetime.strptime(timestring, '%Y-%m-%d %H:%M:%S').timestamp())

# Converts a timestamp from a Google API response to standard YYYY-mm-dd HH:MM:SS format
def convertGoogleTime(gtime):
    parts = gtime.split('T')
    return parts[0] + ' ' + parts[1].split('.')[0]

# Writes "msg" to the logfile on a new line (if LOGGING is set)
def writeLog(msg):
    global LOGFILE
    if LOGGING:
        LOGFILE.write("[" + getCurrentTime(False) + "] " + msg + "\n")

## PROGRAM ##
if __name__ == '__main__':
    main()
